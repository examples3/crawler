# A simple product crawler for Czc.cz website. 

todo:
- finish tests
- implement blackfire
- implement CI

How to run application on localhost:
===
git clone
```
git clone git@gitlab.com:examples3/crawler.git
```
switch to project dir 
```
cd crawler
```
build docker image and run container
```
docker-compose up -d --build
```
install dependencies
```
docker exec -it crawler bash -c "composer install"
```
run app 
```
docker exec -it crawler bash -c "php run.php"
```

Useful commands:
==

Run tests:
```
docker exec -it crawler bash -c "./vendor/bin/phpunit"
```

PHPSTAN
```
docker exec -it crawler bash -c "php ./vendor/bin/phpstan analyse src --level 7"
```

php-fixer
```
docker exec -it crawler bash -c "php php-cs-fixer-v2.phar fix"
```