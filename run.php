<?php

require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;
use App\PageLoader\PageLoader;
use App\Crawler\CzcProductCrawler;
use App\Output\ConsoleOutput;
use App\Dispatcher;

$httpClient = new Client([
    'http_errors' => false,
    'timeout' => 5,
    'connect_timeout' => 5,
    'allow_redirects' => true
]);

$pageLoader = new PageLoader($httpClient);
$crawler = new CzcProductCrawler($pageLoader);
$output = new ConsoleOutput();

// code here
$dispatcher = new Dispatcher($crawler, $output);
$dispatcher->run();

