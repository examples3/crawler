<?php
declare(strict_types=1);
namespace App\Output;

use App\Arrayable;

interface IOutput
{
    /**
     * @return string
     */
    public function getJson(): string;

    /**
     * @param \Traversable<\JsonSerializable> $collection
     */
    public function setData(\Traversable $collection): void;

    /**
     * @return void
     */
    public function display(): void ;

    /**
     * @param \Throwable $e
     */
    public function writeError(\Throwable $e): void;
}
