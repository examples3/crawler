<?php
declare(strict_types=1);
namespace App\Output;

use App\Arrayable;

/**
 * Class ConsoleOutput
 * @package App\Output
 */
class ConsoleOutput implements IOutput
{
    /**
     * @var \Traversable<\JsonSerializable>|null $collection
     */
    private $collection;

    /**
     * @param \Traversable<\JsonSerializable> $collection
     */
    public function setData(\Traversable $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        if (null === $this->collection) {
            return '';
        }

        return json_encode($this->collection, JSON_THROW_ON_ERROR);
    }

    public function display(): void
    {
        echo $this->getJson() . PHP_EOL;
    }

    public function writeError(\Throwable $error): void
    {
        print_r([
            'error' => [
                'message' => $error->getMessage(),
                'code' => $error->getCode()
            ]
        ]);
    }
}
