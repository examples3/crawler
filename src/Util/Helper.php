<?php
declare(strict_types=1);
namespace App\Util;

/**
 * Class Strings
 * @package App\Util
 */
class Helper
{
    /**
     * Removes special characters from string
     *
     * @param string $string
     * @return string
     */
    public static function toPlainText(string $string): string
    {
        return preg_replace('/[^A-Za-z0-9\-\.,_:\s+]/', '', trim($string));
    }

    /**
     * Gets a number from string
     *
     * @param string $string
     * @return float|null
     */
    public static function grabNumber(string $string): ?float
    {
        $number = preg_replace('/[^0-9,\.]/', '', $string);

        if (!is_numeric($number)) {
            return null;
        }

        return (float)$number;
    }

    /**
     * Check up is the $subject is included in the text
     *
     * @param string $subject
     * @param string $text
     * @return bool
     */
    public static function containsText(string $subject, string $text): bool
    {
        return (bool)preg_match("/${subject}/", $text);
    }
}
