<?php
declare(strict_types=1);
namespace App\Entity\Collection;

use App\Entity\Product;

/**
 * Class Products
 * @extends \ArrayObject<string, Product>
 */
class Products extends \ArrayObject
{
    /**
     * @param string $index
     * @param Product $value
     */
    public function offsetSet($index, $value): void
    {
        if (!$value instanceof Product) {
            throw new \InvalidArgumentException('Value must be an instance of Product');
        }

        parent::offsetSet($index, $value);
    }

    /**
     * @param Product $value
     * @return void
     */
    public function append($value): void
    {
        $this->offsetSet($value->getProductId(), $value);
    }
}
