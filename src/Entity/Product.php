<?php
declare(strict_types=1);
namespace App\Entity;

/**
 * Class Product
 * @package App\Lists
 */
final class Product implements \JsonSerializable
{
    /**
     * @var string
     */
    private string $productId;
    /**
     * @var float
     */
    private float $price;
    /**
     * @var int
     */
    private int $rating;
    /**
     * @var string
     */
    private string $name;

    /**
     * Product constructor.
     * @param string $productId
     * @param string $name
     * @param float $price
     * @param int $rating
     */
    public function __construct(string $productId, string $name, float $price, int $rating)
    {
        $this->productId = $productId;
        $this->name = $name;
        $this->price = $price;
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return array<string, float|string|int>
     */
    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'price' =>$this->price,
            'rating' => $this->rating
        ];
    }
}
