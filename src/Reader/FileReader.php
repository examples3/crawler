<?php
declare(strict_types=1);
namespace App\Reader;

/**
 * Reads a content from txt file
 *
 * Class FileReader
 * @package App\Reader
 */
class FileReader implements IReader
{
    /**
     * @var string
     */
    private string $filePath;

    /**
     * FileReader constructor.
     * @param string $filePath
     * @throws ExtensionException
     */
    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        if ('txt' !== $fileExtension) {
            throw new ExtensionException('The file is not in a txt format.');
        }
    }

    /**
     * Reads a file content
     *
     * @return \Generator<string>
     */
    public function read(): \Generator
    {
        $file = new \SplFileObject($this->filePath);
        while (!$file->eof()) {
            $row = $file->fgets();
            if (empty($row)) {
                continue;
            }

            yield $row;
        }

        $file = null;
    }
}
