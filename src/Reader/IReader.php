<?php
declare(strict_types=1);
namespace App\Reader;

interface IReader
{
    /**
     * @return \Generator<string>
     */
    public function read(): \Generator;
}
