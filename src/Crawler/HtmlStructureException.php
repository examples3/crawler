<?php
declare(strict_types=1);
namespace App\Crawler;

use Throwable;

/**
 * Exception when an aimed web page changed a HTML structure
 */
class HtmlStructureException extends \Exception
{
    public function __construct(string $message = 'Html structure was probably changed', int $code = 409, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
