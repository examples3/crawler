<?php
declare(strict_types=1);
namespace App\Crawler;

use Throwable;

/**
 * Exception when a product was not found
 */
class ProductNotFoundException extends \Exception
{
    public function __construct(string $message = 'Product not found', int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
