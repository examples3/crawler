<?php
declare(strict_types=1);
namespace App\Crawler;

use App\Entity\Product;
use App\PageLoader\IPageLoader;
use App\Util\Helper;

/**
 * Gets a product info from www.czc.cz website.
 *
 * Class CzcProductGrabber
 * @package App\Grabber
 */
class CzcProductCrawler implements IGrabber
{
    private const BASE_URL = 'https://www.czc.cz';
    private const SEARCH_URL = self::BASE_URL . '/%s/hledat';

    /**
     * @var IPageLoader
     */
    private IPageLoader $pageLoader;

    /**
     * CzcProductGrabber constructor.
     * @param IPageLoader $pageLoader
     */
    public function __construct(IPageLoader $pageLoader)
    {
        $this->pageLoader = $pageLoader;
    }

    /**
     * Builds a product entity.
     *
     * @param string $productId
     * @return Product
     * @throws HtmlStructureException
     * @throws ProductNotFoundException
     */
    public function getProduct(string $productId): Product
    {
        $url = $this->findProductUrl($productId);
        $pageContent = $this->getProductPageContent($url, $productId);

        return new Product(
            $productId,
            $this->grabName($pageContent),
            $this->grabPrice($pageContent),
            $this->grabRating($pageContent)
        );
    }

    /**
     * Tries to find the product url on the search page.
     *
     * @param string $productId
     * @return string
     * @throws HtmlStructureException
     * @throws ProductNotFoundException
     */
    private function findProductUrl(string $productId): string
    {
        $searchPage = $this->pageLoader->load(sprintf(self::SEARCH_URL, $productId));

        $aTagElements = $searchPage->query('//a[@class="tile-link"]');
        if (!$aTagElements instanceof \DOMNodeList) {
            throw new ProductNotFoundException(sprintf('Product with id %s was not found!', $productId));
        }

        $aTagEl = $aTagElements->item(0);
        if (!$aTagEl instanceof \DOMElement) {
            throw new ProductNotFoundException(sprintf('Product with id %s was not found!', $productId));
        }

        $url = $aTagEl->getAttribute('href');
        if (empty($url)) {
            throw new HtmlStructureException('Missing or empty href attribute!');
        }

        return self::BASE_URL . $url;
    }

    /**
     * Loads the product page and check up if the productId is included in the relevant snippet!
     *
     * @param string $url
     * @param string $productId
     * @return \DomXPath
     * @throws HtmlStructureException
     * @throws ProductNotFoundException
     */
    private function getProductPageContent(string $url, string $productId): \DOMXPath
    {
        $productPage = $this->pageLoader->load($url);
        $productParamsEl = $productPage->query('//div[@id="pd-parameter"]');

        if (!$productParamsEl instanceof \DOMNodeList || $productParamsEl->length < 1) {
            throw new HtmlStructureException();
        }

        $text = (string)($productParamsEl->item(0))->textContent;

        if (false === Helper::containsText($productId, $text)) {
            throw new ProductNotFoundException(sprintf('Product with id %s was not found!', $productId));
        }

        return $productPage;
    }

    /**
     * Grabs price from \DomXPath
     *
     * @param \DomXPath $pageContent
     * @return float
     * @throws HtmlStructureException
     */
    private function grabPrice(\DomXPath $pageContent): float
    {
        $priceEl = $pageContent->query('//span[@class="price action"]//span[@class="price-vatin"]');

        if (!$priceEl instanceof \DOMNodeList || $priceEl->length < 1) {
            throw new HtmlStructureException('Price element was not found');
        }

        $price = Helper::grabNumber($priceEl->item(0)->textContent);
        if (null === $price) {
            throw new HtmlStructureException('Price could not be found');
        }

        return  $price;
    }

    /**
     * Grabs rating from \DomXPath
     *
     * @param \DomXPath $pageContent
     * @return int
     * @throws HtmlStructureException
     */
    private function grabRating(\DomXPath $pageContent): int
    {
        $ratingEl = $pageContent->query('//span[@class="rating__label"]');

        if (!$ratingEl instanceof \DOMNodeList || $ratingEl->length < 1) {
            throw new HtmlStructureException('Rating element was not found');
        }

        $rating = Helper::grabNumber($ratingEl->item(0)->textContent);
        if (null === $rating) {
            throw new HtmlStructureException('Rating could not be found');
        }

        return  (int)$rating;
    }

    /**
     * Grabs name from \DomXPath
     *
     * @param \DomXPath $pageContent
     * @return string
     * @throws HtmlStructureException
     */
    private function grabName(\DomXPath $pageContent): string
    {
        $titleEl = $pageContent->query('//div[@id="product-detail"]//h1');

        if (!$titleEl instanceof \DOMNodeList || $titleEl->length < 1) {
            throw new HtmlStructureException('Title element was not found');
        }

        $title = Helper::toPlainText($titleEl->item(0)->textContent);
        if (empty($title)) {
            throw new HtmlStructureException('Title could not be found');
        }

        return  $title;
    }
}
