<?php
declare(strict_types=1);
namespace App\Crawler;

use App\Entity\Product;

interface IGrabber
{
    /**
     * @param string $productId
     * @return Product
     */
    public function getProduct(string $productId): Product;
}
