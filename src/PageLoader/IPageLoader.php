<?php
declare(strict_types=1);
namespace App\PageLoader;

interface IPageLoader
{
    public function load(string $url): \DomXPath;
}
