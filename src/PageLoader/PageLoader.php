<?php
declare(strict_types=1);
namespace App\PageLoader;

use \GuzzleHttp\ClientInterface;

/**
 * Class HtmlLoader
 * @package App\HtmlLoader
 */
class PageLoader implements IPageLoader
{
    /**
     * @var ClientInterface
     */
    private ClientInterface $httpClient;

    /**
     * HtmlLoader constructor.
     * @param ClientInterface $httpClient
     */
    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $url
     * @return \DomXPath
     * @throws ConnectException
     * @throws PageNotFoundException
     * @throws ServerException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function load(string $url): \DomXPath
    {
        try {
            $response = $this->httpClient->request('GET', $url);
        } catch (\GuzzleHttp\Exception\ConnectException $e) {
            throw new ConnectException($e->getMessage(), $e->getCode(), $e);
        }

        $statusCode = $response->getStatusCode();

        if ($statusCode > 399 && $statusCode < 499) {
            throw new PageNotFoundException("Page with url '${url}' was not found ");
        }

        if ($statusCode > 499 && $statusCode < 599) {
            throw new ServerException("Url '${url}' unavailable. Page is done.");
        }

        $dom = new \DOMDocument();
        $dom->loadHTML((string)$response->getBody(), LIBXML_NOWARNING | LIBXML_NOERROR);

        return new \DomXPath($dom);
    }
}
