<?php
declare(strict_types=1);
namespace App\PageLoader;

use Throwable;

/**
 * Exception when a server error is encountered (4xx codes)
 */
class PageNotFoundException extends \Exception
{
    public function __construct(string $message = 'Page was not found', int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
