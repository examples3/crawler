<?php
declare(strict_types=1);
namespace App\PageLoader;

use Throwable;

/**
 * Exception when a server error is encountered (5xx codes)
 */
class ServerException extends \Exception
{
    public function __construct(string $message = 'Server error', int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
