<?php
declare(strict_types=1);
namespace App\PageLoader;

use Throwable;

/**
 * Exception when a is operation timed out
 */
class ConnectException extends \Exception
{
    public function __construct(string $message = 'Operation timed out', int $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
