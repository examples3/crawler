<?php
declare(strict_types=1);
namespace App;

use App\Entity\Collection\Products;
use App\Crawler\ProductNotFoundException;
use App\Crawler\IGrabber;
use App\PageLoader\ConnectException;
use App\Reader\FileReader;
use App\Output\IOutput;
use App\Util\Helper;

/**
 * Class Dispatcher
 * @package App
 */
class Dispatcher
{
    /**
     * @var IGrabber
     */
    private $grabber;
    /**
     * @var IOutput
     */
    private $output;

    /**
     * @param IGrabber $grabber
     * @param IOutput $output
     */
    public function __construct(IGrabber $grabber, IOutput $output)
    {
        $this->grabber = $grabber;
        $this->output = $output;
    }

    /**
     * @throws Reader\ExtensionException
     */
    public function run(): void
    {
        $products = new Products();

        $fileReader = new FileReader(__DIR__ . '/../vstup.txt');

        foreach ($fileReader->read() as $line) {
            try {
                $productId = Helper::toPlainText($line);
                $product = $this->grabber->getProduct($productId);

                $products->append($product);
            } catch (ConnectException | ProductNotFoundException $e) {
                $this->output->writeError($e);
            }
        }

        $this->output->setData($products);
        $this->output->display();
    }
}
