<?php
declare(strict_types=1);
namespace Tests\Crawler;

use App\Crawler\CzcProductCrawler;
use App\Crawler\HtmlStructureException;
use App\Crawler\ProductNotFoundException;
use App\Entity\Product;
use App\PageLoader\PageLoader;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class CzcProductCrawlerTest
 * @package Tests\Crawler
 */
final class CzcProductCrawlerTest extends TestCase
{
    private const PRODUCT_ID = 'BX80662E31230V5';

    /**
     * @var MockObject
     */
    private $pageLoader;

    /**
     * @before
     */
    protected function init()
    {
        $this->pageLoader = $this->getMockBuilder(PageLoader::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testGetProductEntity()
    {
        $this->pageLoader
            ->expects($this->exactly(2))
            ->method('load')
            ->will(
                $this->onConsecutiveCalls(
                    $this->getExpectedSearchPage(),
                    $this->getExpectedProductPage(self::PRODUCT_ID)
                )
            );

        $crawler = new CzcProductCrawler($this->pageLoader);
        $product = $crawler->getProduct(self::PRODUCT_ID);

        $this->assertInstanceOf(Product::class, $product);
    }

    public function testProductNotFoundException()
    {
        $this->expectException(ProductNotFoundException::class);

        $this->pageLoader
            ->expects($this->once())
            ->method('load')
            ->willReturn($this->getEmptyPage());

        $crawler = new CzcProductCrawler($this->pageLoader);
        $crawler->getProduct(self::PRODUCT_ID);
    }

    public function testHtmlStructureException()
    {
        $this->expectException(HtmlStructureException::class);

        $this->pageLoader
            ->expects($this->exactly(2))
            ->method('load')
            ->will(
                $this->onConsecutiveCalls(
                    $this->getExpectedSearchPage(),
                    $this->getUnexpectedProductPage(self::PRODUCT_ID)
                )
            );

        $crawler = new CzcProductCrawler($this->pageLoader);
        $crawler->getProduct(self::PRODUCT_ID);
    }

    private function getEmptyPage(): \DOMXPath
    {
        $html = '<html lang="cs"><body></body></html>';
        return $this->buildDomXPath($html);
    }

    private function getExpectedSearchPage(): \DOMXPath
    {
        $html = '<html><body>';
        $html .= '<a class="tile-link" href="/product">link to product</a><';
        $html .= '</body></html>';

        return $this->buildDomXPath($html);
    }

    private function getExpectedProductPage(string $productId): \DOMXPath
    {
        $html = '<html><body>';
        $html .= '<div id="product-detail">';
        $html .= '<h1>Name of product</h1>';
        $html .= sprintf('<div id="pd-parameter">%s</div>', $productId);
        $html .= '<span class="price action"><span class="price-vatin">3 330Kč</span></span>';
        $html .= '<span class="rating__label">95</span>';
        $html .= '</div>';
        $html .= '</body></html>';

        return $this->buildDomXPath($html);
    }

    private function getUnexpectedProductPage(string $productId): \DOMXPath
    {
        $html = '<html><body>';
        $html .= '<div id="product-detail">';
        $html .= '<h1>Name of product</h1>';
        $html .= sprintf('<div id="pd-parameter">%s</div>', $productId);
        $html .= '<span>3 330Kč</span>';
        $html .= '<span>95</span>';
        $html .= '</div>';
        $html .= '</body></html>';

        return $this->buildDomXPath($html);
    }

    /**
     * @param string $html
     * @return \DOMXPath
     */
    private function buildDomXPath(string $html): \DOMXPath
    {
        $dom = new \DOMDocument();
        $dom->loadHTML($html, LIBXML_NOWARNING | LIBXML_NOERROR);

        return new \DomXPath($dom);
    }
}
